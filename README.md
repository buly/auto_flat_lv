**Automation Port of [Marta Nuñez-Garcia's LV Flattening algorithm.](https://github.com/martanunez/LV_flattening)**

## INSTALLATION

``` console
_>: pip install .
```

## `flat_lv.get_seeds`: Automate seeds selection

*Note: Only work with SAX oriented images*

+ Apex
  - Get as centroid of the last 3 slices of the lvwall image 
  - Project/FindClosest on the lvwall's surface mesh to get position on the lvwall

+ Theta Zero
  - Get centroid of rvepi
  - Project/FindClosest on lvwall surface mesh
  - Project/FindClosest on mitral valve (mv) mesh
  - Project/FindClosest on lvwall surface mesh (Get the exact point on the base)

+ Theta Aux
  - rotate in z axis of index coordinate (not the physical space) the vector from mv_centroid to theta zero by 2 degree
  - Project/FindClosest on lvwall surface mesh


## USAGE 
- `flat_lv.automate`: Automate function; From sitk to flat\_lv.
``` python
import SimpleITK as sitk

fron auto_flat_lv import flat_lv as fl

lvepi_img = sitk.ReadImage("/path/to/file")
lvendo_img = sitk.ReadImage("/path/to/file")
rvepi_img = sitk.ReadImage("/path/to/file")
thickness_img = sitk.ReadImage("/path/to/file")

output_shape = 256 # Numpy array output shape 

output_dict = flat_lv.automate(lvepi_img=lvepi_img, 
    lvendo_img=lvendo_img, 
    rvepi_img=rvepi_img,
    thickness_img=thickness_img,
    output_shape=bulleye_output_shape,
    )

flat_lv = output_dict['flat_lv'] # polydata
flat_lv_np = output_dict['flat_lv_np'] # (output_shape x output_shape)

# Genereated meshes: for remap and debugging purpose.
lvwall_contour_mesh = output_dict['lvwall_contour_mesh']
mv_mesh = output_dict['mv_mesh']
rvepi_mesh = output_dict['rvepi_mesh']
```

- `flat_lv.get_flat_lv_np`: generate numpy image from flat_lv mesh
```python
from auto_flat_lv import flat_lv as fl
from auto_flat_lv import aux_functions as af

# read mesh
# ..note: the mesh must contain at least one scalar\_value
flat_lv_mesh = af.readvtk("/path/to/file")

flat_lv_np = fl.get_flat_lv_np(polydata=flat_lv_mesh, 
				output_shape=256, # set desire output image shape
				scalar_name='Thickness', # change the scalar_name accordingly
				)
```

- `flat_lv.remap`: Remap numpy array
``` python
import numpy as np
import SimpleITK as sitk

from auto_flat_lv import flat_lv as fl
from auto_flat_lv import aux_functions as af

# read
flat_mesh = af.readvtk("/path/to/file")
lvwall_mesh = af.readvtk("/path/to/file")

array = np.load("")

remap_mesh_2d, remap_mesh_3d = fl.remap(
    array=array,
    disk_mesh=flat_mesh, 
    surface_mesh=lvwall_mesh, 
    array_name='remap', # used as scalar name
    )
```


