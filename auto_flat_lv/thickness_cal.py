import vtk
import numpy as np 
# from . import aux_functions as af


def cellthreshold(polydata, arrayname, start=0, end=1):
    threshold = vtk.vtkThreshold()
    threshold.SetInputData(polydata)
    threshold.SetInputArrayToProcess(0, 0, 0, vtk.vtkDataObject.FIELD_ASSOCIATION_CELLS, arrayname)
    threshold.ThresholdBetween(start, end)
    threshold.Update()

    surfer = vtk.vtkDataSetSurfaceFilter()
    surfer.SetInputData(threshold.GetOutput())
    surfer.Update()
    return surfer.GetOutput()


def surfacearea(polydata):
    properties = vtk.vtkMassProperties()
    properties.SetInputData(polydata)
    properties.Update()
    return properties.GetSurfaceArea()


def point2celldata(polydata):
    """
    """
    converter = vtk.vtkPointDataToCellData()
    converter.SetInputData(polydata)
    converter.Update()
    return converter.GetOutput()


def automate(polydata, 
        threshold_range=list(range(2, 6)),
        bins=False,
        arrayname='thickness', 
        debug=False,
        ):
    """
    :returns: dictionary containing area of each threshold val {threshold_val: area}
    :rtype: dict
    """
    # convert to 
    cell_data = point2celldata(polydata)
    areas = {val: 0 for val in threshold_range}
    # np.zeros(threshold_range)
    thres_vtk = {val: None for val in threshold_range}
    for ind, val in enumerate(threshold_range): 
        start_val = threshold_range[ind-1] if bins and ind != 0 else 0
        clipped = cellthreshold(polydata=cell_data, 
                arrayname=arrayname, 
                start=start_val, 
                end=val,
                )        

        thres_vtk[val] = clipped
        if clipped.GetNumberOfPoints() > 0:
            areas[val] = np.divide(surfacearea(clipped), 100)   # cm2

    if debug: 
        return areas, thres_vtk

    return areas
